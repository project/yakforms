# What is this module ?

Yakforms provides a fully working form service. It is aimed to work as part of the [Yakforms distribution](https://framagit.org/yakforms/yakforms) for Drupal 7 (the module alone is not usable).

Yakforms defines the following items for the website :

* A new **node type** : « form » or form1 (machine-name) nodes are publicly-accessible nodes that any authenticated user can create and configure. A webform is attached to each of node.
* Blocks and menu links for user navigation.
* Default pages for documentation, landing, etc.
* Cron tasks for limited-lifetime forms : to avoid piling up useless data forever, forms expire after a few months by default. (This feature can be deactivated by the admin if necessary).
* A central administration menu to manage most of the website's parameters.

Though all releases are made available on Drupal.org to simplify the update process, the actual development takes place on [Framagit](https://framagit.org/framasoft/framaforms) for now.

# Some history
Yakforms was previously named `framaforms`, and was originally developped by French non-profit [Framasoft](https://framasoft.org/en/) during their [De-googleify Internet campaign](https://degooglisons-internet.org/en/).

# Where can I try Yakforms ?
A demonstration instance is available on [Framaforms.org](https://framaforms.org/).

# How can I contribute ?
Any help to develop this module is welcome !

The contribution guidelines are detailed [on the website](https://yakforms.org/en/pages/contribute.html).

You can get in touch on [Framacolirbi](https://framacolibri.org/c/yakforms), a community help forum around free (as in freedom) software and culture.

# Credits
Yakforms makes extensive use of [Webform](https://www.drupal.org/project/webform/) and [Form builder](https://www.drupal.org/project/form_builder/), along other modules. These modules are included in the distribution, and are required for this module.
